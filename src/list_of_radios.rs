extern crate yaml_rust;
use std::fs;
use yaml_rust::{Yaml, YamlLoader};

use lazy_static::lazy_static;

lazy_static! {
    static ref YAML: Vec<Yaml> = load_radios();
}

fn load_radios() -> Vec<Yaml> {
    let yaml_file = fs::read_to_string("/usr/local/share/radios.yml").unwrap();
    // let yaml_file = fs::read_to_string("src/radios.yml").unwrap(); // débugging
    YamlLoader::load_from_str(&yaml_file).unwrap()
}

pub fn radio_url(name: &str) -> &str {
    let doc = &YAML[0];

    match doc[&name[..]].as_str() {
        None => "",
        _ => doc[&name[..]].as_str().unwrap(),
    }
}

pub fn list_of_radios() -> Vec<&'static str> {
    let mut vec = Vec::new();

    for k in YAML.iter() {
        let hash = k.as_hash().unwrap();
        for (k, _) in hash {
            let key = k.as_str().unwrap();
            vec.push(key);
        }
    }

    vec
}
