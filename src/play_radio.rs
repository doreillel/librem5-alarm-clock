use lazy_static::lazy_static;
use std::error::Error;
use std::result::Result;
use std::sync::Mutex;
use stream_download::storage::temp::TempStorageProvider;
use stream_download::{Settings, StreamDownload};

lazy_static! {
    static ref FLAG: Mutex<u8> = Mutex::new(Stream::Unactive as u8);
}

enum Stream {
    Running = 0,
    Stopped = 1,
    Unactive = 2,
}

#[tokio::main]
pub async fn play_radio(url: &str) -> Result<(), Box<dyn Error + Send + Sync>> {
    let reader: StreamDownload<TempStorageProvider> = StreamDownload::new_http(
        url.parse()?,
        TempStorageProvider::new(),
        Settings::default(),
    )
    .await?;

    if *(FLAG.lock().unwrap()) == Stream::Running as u8 {
        return Ok(());
    }

    let _ = tokio::task::spawn_blocking(move || {
        let (_stream, handle) = rodio::OutputStream::try_default()?;
        let sink = rodio::Sink::try_new(&handle)?;
        sink.append(rodio::Decoder::new(reader)?);

        *(FLAG.lock().unwrap()) = Stream::Running as u8;
        while *(FLAG.lock().unwrap()) == Stream::Running as u8 {
            //sink.sleep_until_end();
        }

        sink.stop();
        drop(sink);
        drop(_stream);
        drop(handle);
        *(FLAG.lock().unwrap()) = 2;

        Ok::<_, Box<dyn Error + Send + Sync>>(())
    })
    .await;

    Ok(())
}

pub fn stop_radio() {
    let mut flag = FLAG.lock().unwrap();
    *flag = Stream::Stopped as u8;
}
