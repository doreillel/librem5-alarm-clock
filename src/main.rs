mod list_of_radios;
mod play_radio;

use duct::cmd;
use list_of_radios::{list_of_radios, radio_url};
use play_radio::{play_radio, stop_radio};

use chrono::prelude::*;
use chrono::{Duration, Local};

use gtk4::{
    gio::{self, ApplicationFlags},
    glib::{self, Propagation},
    prelude::*,
};

fn main() -> glib::ExitCode {
    let application = gtk4::Application::builder()
        .application_id("theDarky.alarm.librem5.gtk4")
        .build();
    application.set_flags(ApplicationFlags::HANDLES_COMMAND_LINE);

    application.connect_activate(build_ui);

    application.connect_command_line(move |app, app_command_line| -> i32 {
        let args = app_command_line.arguments();

        if args.len() > 1 {
            gio::spawn_blocking(move || {
                let radio_name = &args[1];

                if !radio_name.is_empty() {
                    let radio_url_string = radio_url(radio_name.to_str().unwrap());

                    if !radio_url_string.is_empty() {
                        let _ = play_radio(radio_url_string);
                    }
                }
            });
        }

        app.activate();

        1
    });

    application.run()
}

fn two_zero_for_spin_buttons(value: f64) -> String {
    format!("{:02}", value)
}

fn build_ui(application: &gtk4::Application) {
    let ui_src = include_str!("alarm.ui");
    let builder = gtk4::Builder::from_string(ui_src);

    let window = builder
        .object::<gtk4::ApplicationWindow>("window")
        .expect("Couldn't get window");
    window.set_application(Some(application));

    // add the drop down list
    let radios = list_of_radios();

    let model_drop_down: gtk4::StringList = gtk4::StringList::new(&[]);

    for value in radios.iter().copied() {
        model_drop_down.append(value);
    }

    let dt = Local::now();
    let current_hour = dt.hour() as f64;
    let current_minute = dt.minute() as f64;

    // display hours with 01, 02 ... 23 format
    let hours = builder
        .object::<gtk4::SpinButton>("hours")
        .expect("Couldn't get spinbutton");
    hours.set_value(current_hour);

    hours.connect_output(move |hours| {
        let value = hours.value();
        hours.set_text(two_zero_for_spin_buttons(value).as_str());
        Propagation::Stop
    });

    // display hours with 01, 02 ... 59 format
    let minutes = builder
        .object::<gtk4::SpinButton>("minutes")
        .expect("Couldn't get spinbutton");
    minutes.set_value(current_minute);

    minutes.connect_output(move |minutes| {
        let value = minutes.value();
        minutes.set_text(two_zero_for_spin_buttons(value).as_str());
        Propagation::Stop
    });

    let drop_down_radio = builder
        .object::<gtk4::DropDown>("listOfRadios")
        .expect("Couldn't get dropdown");

    drop_down_radio.set_model(Some(&model_drop_down));

    let save_alarm = builder
        .object::<gtk4::Button>("saveAlarm")
        .expect("Couldn't get button");

    let play_button = builder
        .object::<gtk4::Button>("playRadioButton")
        .expect("Couldn't get button");

    let stop_button = builder
        .object::<gtk4::Button>("stopRadioButton")
        .expect("Couldn't get button");

    let dropdown_play= drop_down_radio.clone();
    let radios_play = radios.clone();
    play_button.connect_clicked(move |_| {
        println!("play_button");

        let selected = dropdown_play.selected();
        let chosen_radio = radios_play[selected as usize];
        let radio_url_string = radio_url(chosen_radio);

        gio::spawn_blocking(move || {
            let _ = play_radio(radio_url_string);
        });
    });

    stop_button.connect_clicked(move |_| {
        println!("stop_button");
        gio::spawn_blocking(move || {
            stop_radio()
        });
    });

    let dropdown_save= drop_down_radio.clone();
    let radios_save = radios.clone();
    save_alarm.connect_clicked(move |_| {
        let chosen_hour = hours.value() as u32;
        let chosen_minute = minutes.value() as u32;
        let selected = dropdown_save.selected();
        let chosen_radio = radios_save[selected as usize];

        let command = format!(
            "echo \"export DISPLAY=:0 && killall -9 alarm && alarm -- {}\" | at {}:{}",
            &chosen_radio, &chosen_hour, &chosen_minute
        );

        let command2 = calculate_rtc_wake(&chosen_hour, &chosen_minute);

        gio::spawn_blocking(move || {
            let _start_radio = cmd!("bash", "-c", command).run();
            let _awake_phone = cmd!("bash", "-c", command2).run();
        });
    });

    window.present();
}


fn calculate_rtc_wake(hour: &u32, min: &u32)->String {
    let now = Local::now();
    let mut date_time = now.date_naive().and_hms_opt(*hour, *min, 0).unwrap();

    if now.time() > date_time.time() {
        date_time = date_time + Duration::days(1);
    }

    date_time = date_time - Duration::minutes(1);

    String::from("/usr/sbin/rtcwake -m no -l --date ") + (&date_time.format("%Y%m%d%H%M40").to_string())
}