# Alarm

## How to compile it

To compile the Rust code, you need to install qemu on your machine so that the aarch64 architecture is emulated, and a Docker container for this architecture is functional.

For Ubuntu 22.04 (and Debian-like systems), type the following:

``` 
sudo apt-get install qemu binfmt-support qemu-user-static
``` 

Then you can create the image to develop for your Librem 5:

``` 
docker build . -t ldoreille-qemu -f Dockerfile.qemu
```

And connect to it with the following command to initiate the compilation:

```
docker run --rm -ti -v `pwd`:/app ldoreille
```