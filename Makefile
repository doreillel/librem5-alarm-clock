default:
	@@echo "Only two options: \"packing\" and \"compiling\""
packing:
	cd ./package && dpkg-deb --build librem5-alarm-clock	
compiling:
	rm -rf target/release && cargo build --release  && cp target/release/alarm librem5-alarm-clock/usr/local/bin/alarm